var dssv = [];
const DSSV_LOCAL = "DSSV_LOCAL";
var dssvJson = localStorage.getItem(DSSV_LOCAL);
if (dssvJson != null) {
  dssv = JSON.parse(dssvJson);
  for (var index = 0; index < dssv.length; index++) {
    sv = dssv[index];
    dssv[index] = new SinhVien(
      sv.ten,
      sv.ma,
      sv.matKhau,
      sv.email,
      sv.toan,
      sv.ly,
      sv.hoa
    );
  }
  renderDSSV(dssv);
}

function themSV() {
  var newSV = info();
  if (
    same("txtMaSV", "spanMaSV", "Mã sinh viên đã sử dụng") == false ||
    validation() == false
  ) {
    return;
  } else {
    dssv.push(newSV);
    var dssvJson = JSON.stringify(dssv);
    localStorage.setItem(DSSV_LOCAL, dssvJson);
    renderDSSV(dssv);
  }
}
function xoasv(id) {
  var index = tim(id, dssv);
  dssv.splice(index, 1);
  var dssvJson = JSON.stringify(dssv);
  localStorage.setItem(DSSV_LOCAL, dssvJson);
  renderDSSV(dssv);
}

function edit(id) {
  document.getElementById("txtMaSV").disabled = true;
  document.getElementById("them").style.display = "none";
  var index = tim(id, dssv);
  if (index != -1) {
    var sv = dssv[index];
    showInfo(sv);
  }
}

function update() {
  if (validation() == false) {
    return;
  } else {
    if ((document.getElementById("txtMaSV").disabled = false)) {
      return;
    }
    var a = document.getElementById("txtMaSV").value * 1;
    var index = tim(a, dssv);
    if (index == -1) {
      resetAll();
      return;
    } else {
      dssv.splice(index, 1, info());
      var dssvJson = JSON.stringify(dssv);
      localStorage.setItem(DSSV_LOCAL, dssvJson);
      renderDSSV(dssv);
      resetAll();
    }
  }
}
function find() {
  var ten = getID("txtSearch").value;
  if (ten == "") {
    alert("Xin nhập tên sinh viên");
    return;
  } else {
    var a = timTen("txtSearch", dssv);
    if (a.length == 0) {
      alert("Sinh viên không tồn tại");
    } else {
      var find = [];
      for (var i = 0; i < a.length; i++) {
        var k = a[i];
        find.push(dssv[k]);
      }
      renderDSSV(find);
    }
  }
  document.getElementById("addBt").style.display = "block";
}

function resetButton() {
  resetAll();
}

function SinhVien(ten, ma, matKhau, email, toan, ly, hoa) {
  this.ten = ten;
  this.ma = ma;
  this.matKhau = matKhau;
  this.email = email;
  this.toan = toan;
  this.ly = ly;
  this.hoa = hoa;
  this.dtb = function () {
    var a = (this.toan * 1 + this.ly * 1 + this.hoa * 1) / 3;

    return a.toFixed(1);
  };

  this.mangThuocTinh = [
    this.ten,
    this.ma,
    this.matKhau,
    this.email,
    this.toan,
    this.ly,
    this.hoa,
  ];
}

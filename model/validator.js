function validation() {
    if (checkAllInput() != 1) {
        return false;
    } else if (checkData() != 1) {
        return false;
    } else {
        return true;
    }
}
function checkData() {
    var a =
        checkEmail("txtEmail", "spanEmailSV", "Email không hợp lệ") &
        checkLength("txtMaSV", "spanMaSV", "Mã sinh viên gồm 4 đến 8 kí tự", 4, 8) &
        checkPoint("txtDiemToan", "spanToan", "Điểm lớn hơn hoặc bằng 0") &
        checkPoint("txtDiemHoa", "spanHoa", "Điểm lớn hơn hoặc bằng 0") &
        checkPoint("txtDiemLy", "spanLy", "Điểm lớn hơn hoặc bằng 0") &
        checkPass(
            "txtPass",
            "spanMatKhau",
            "Khôg hợp lệ, nhập pass từ 6-15 kí tự, ít nhất 1 kí tự đặc biệt, số và chữ hoa"
        ) &
        checkTxt("txtTenSV", "spanTenSV", "Tên không hợp lệ");
    return a;
}
function checkEmpty(input, span) {
    var b = getID(input).value;
    if (b == "") {
        getID(span).innerHTML = `Xin hãy nhập`;
        return false;
    } else {
        getID(span).innerHTML = "";
        return true;
    }
}
function checkAllInput() {
    var a =
        checkEmpty("txtMaSV", "spanMaSV") &
        checkEmpty("txtTenSV", "spanTenSV") &
        checkEmpty("txtEmail", "spanEmailSV") &
        checkEmpty("txtPass", "spanMatKhau") &
        checkEmpty("txtDiemToan", "spanToan") &
        checkEmpty("txtDiemLy", "spanLy") &
        checkEmpty("txtDiemHoa", "spanHoa");
    return a;
}
function checkLength(id, idError, messenge, min, max) {
    var a = getID(id).value;
    if (a.length < min || a.length > max) {
        document.getElementById(idError).innerText = messenge;
        return false;
    } else {
        document.getElementById(idError).innerText = "";
        return true;
    }
}
function checkEmail(id, idError, messenge) {
    var a = getID(id).value;
    const re =
        /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
    if (re.test(a)) {
        document.getElementById(idError).innerText = "";
        return true;
    } else {
        document.getElementById(idError).innerText = messenge;
        return false;
    }
}
function checkPoint(id, idError, messenge) {
    var a = getID(id).value;
    if (a < 0) {
        document.getElementById(idError).innerText = messenge;
        return false;
    } else {
        document.getElementById(idError).innerText = "";
        return true;
    }
}
function checkPass(id, idError, messenge) {
    var decimal =
        /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[^a-zA-Z0-9])(?!.*\s).{6,15}$/;
    var a = getID(id).value;
    if (a.match(decimal)) {
        document.getElementById(idError).innerText = "";
        return true;
    } else {
        document.getElementById(idError).innerText = messenge;
        return false;
    }
}
function checkTxt(id, idError, messenge) {
    var a = getID(id).value;
    var letter = new RegExp(
        "^[a-zA-ZÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂẾưăạảấầẩẫậắằẳẵặẹẻẽềềểếỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ ]+$"
    );
    if (letter.test(a)) {
        document.getElementById(idError).innerText = "";
        return true;
    } else {
        document.getElementById(idError).innerText = messenge;
        return false;
    }
}
function same(id, idError, messenge) {
    var idSame = getID(id).value;
    if (tim(idSame, dssv) == -1) {
        document.getElementById(idError).innerText = "";
        return true;
    } else {
        document.getElementById(idError).innerText = messenge;
        return false;
    }
}

function getID(a) {
  return document.getElementById(a);
}

function info() {
  const maSV = document.getElementById("txtMaSV").value;
  const tenSV = document.getElementById("txtTenSV").value;
  const email = document.getElementById("txtEmail").value;
  const matKhau = document.getElementById("txtPass").value;

  const diemToan = document.getElementById("txtDiemToan").value;
  const diemLy = document.getElementById("txtDiemLy").value;
  const diemHoa = document.getElementById("txtDiemHoa").value;

  return new SinhVien(tenSV, maSV, matKhau, email, diemToan, diemLy, diemHoa);
}

function renderDSSV(svArr) {
  var content = "";
  for (var i = 0; i < svArr.length; i++) {
    var sv = svArr[i];
    var trContent = `<tr>
    <td>${sv.ma}</td>
    <td>${sv.ten}</td>
    <td>${sv.email}</td>
    <td>${sv.dtb()}</td>
    <td><button onclick="xoasv(${sv.ma
      })" type="button" class="btn btn-danger">Delete</button>
    <button type="button" class="btn btn-primary" onclick="edit(${sv.ma
      })">Edit</button></td>
    
    </tr>`;
    content += trContent;
  }
  document.getElementById("tbodySinhVien").innerHTML = content;
}

function tim(id, dssv) {
  for (var index = 0; index < dssv.length; index++) {
    var sv = dssv[index];
    if (sv.ma == id) {
      return index;
    }
  }
  return -1;
}

function showInfo(sv) {
  document.getElementById("txtMaSV").value = sv.ma;
  document.getElementById("txtTenSV").value = sv.ten;

  document.getElementById("txtEmail").value = sv.email;
  document.getElementById("txtPass").value = sv.matKhau;

  document.getElementById("txtDiemToan").value = sv.toan;
  document.getElementById("txtDiemLy").value = sv.ly;
  document.getElementById("txtDiemHoa").value = sv.hoa;
}
function resetAll() {
  document.getElementById("them").style.display = "inline-block";
  document.getElementById("txtMaSV").value = "";
  document.getElementById("txtTenSV").value = "";
  document.getElementById("txtEmail").value = "";
  document.getElementById("txtPass").value = "";
  document.getElementById("txtDiemToan").value = "";
  document.getElementById("txtDiemLy").value = "";
  document.getElementById("txtDiemHoa").value = "";
  document.getElementById("txtMaSV").disabled = false;
}
function timTen(id, ds) {
  var index = [];
  var tenSearch = document.getElementById(id).value;
  for (var i = 0; i < ds.length; i++) {
    var sv = ds[i];
    if (sv.ten == tenSearch) {
      index.push(i);
    }
  }
  return index;
}
function myshow() {
  var x = document.getElementById("txtPass");
  if (x.type === "password") {
    x.type = "text";
  } else {
    x.type = "password";
  }
}
document.getElementById("addBt").style.display = "none";
